<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Sarah',
                'email' => 'admin@example.com',
                'password' => '$2y$10$2wfwYmyDokmfGsC7lmTtjODmysMqZrN5lC5NgF28II70b9X1trBsW',
                'remember_token' => 'prxcPEpF1boEhVLsmC8F69ViofE9OrXsJqImyBAFYuawYoNh2kVFkYnIqmL4',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}