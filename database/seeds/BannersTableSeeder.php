<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banners')->delete();
        
        \DB::table('banners')->insert(array (
            0 => 
            array (
                'id' => 2,
                'title' => '3',
                'description' => '4',
                'url' => '#',
                'created_at' => '2019-02-08 19:46:44',
                'updated_at' => '2019-02-08 22:45:55',
            ),
        ));
        
        
    }
}