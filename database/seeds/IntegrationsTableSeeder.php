<?php

use Illuminate\Database\Seeder;

class IntegrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('integrations')->delete();
        
        \DB::table('integrations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => 'uploads/integrations/1549667537.png',
                'created_at' => '2019-02-08 23:12:17',
                'updated_at' => '2019-02-08 23:12:17',
            ),
        ));
        
        
    }
}