<?php

use Illuminate\Database\Seeder;

class AboutTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('about')->delete();
        
        \DB::table('about')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => '5',
                'short_description' => '6',
                'content' => '7',
                'image' => 'uploads/AboutPage/1549658963.jpg',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '2019-02-08 20:49:24',
            ),
        ));
        
        
    }
}