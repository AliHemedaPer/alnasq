<?php

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('translations')->delete();
        
        \DB::table('translations')->insert(array (
            0 => 
            array (
                'id' => 4,
                'group_id' => 3,
                'value' => 'Permanent design service',
                'locale' => 'en',
            ),
            1 => 
            array (
                'id' => 5,
                'group_id' => 3,
                'value' => 'خدمة التصاميم المؤقتة',
                'locale' => 'ar',
            ),
            2 => 
            array (
                'id' => 6,
                'group_id' => 4,
                'value' => 'Sign up now with our Instant Design service and save your monthly expenses...',
                'locale' => 'en',
            ),
            3 => 
            array (
                'id' => 7,
                'group_id' => 4,
                'value' => 'قم بالتسجيل الآن باستخدام خدمة تصميمك الفوري واحفظ نفقاتك الشهرية...',
                'locale' => 'ar',
            ),
            4 => 
            array (
                'id' => 8,
                'group_id' => 5,
                'value' => 'ALNASQ AL ARABI CO.',
                'locale' => 'en',
            ),
            5 => 
            array (
                'id' => 9,
                'group_id' => 5,
                'value' => 'النسق العربى.',
                'locale' => 'ar',
            ),
            6 => 
            array (
                'id' => 10,
                'group_id' => 6,
                'value' => 'We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .',
                'locale' => 'en',
            ),
            7 => 
            array (
                'id' => 11,
                'group_id' => 6,
                'value' => 'نعتقد أنه يمكننا تغيير الثقافات وجعل العالم مكانًا أفضل. يجب تحقيق هذا الهدف من خلال إنتاج منتجات ذات جودة يتم من خلالها نقل القيم إلى المجتمع والتأثير فيه .',
                'locale' => 'ar',
            ),
            8 => 
            array (
                'id' => 12,
                'group_id' => 7,
                'value' => '<p>We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .</p>',
                'locale' => 'en',
            ),
            9 => 
            array (
                'id' => 13,
                'group_id' => 7,
                'value' => '<p>نعتقد أنه يمكننا تغيير الثقافات وجعل العالم مكانًا أفضل. يجب تحقيق هذا الهدف من خلال إنتاج منتجات ذات جودة يتم من خلالها نقل القيم إلى المجتمع والتأثير فيه .</p>',
                'locale' => 'ar',
            ),
            10 => 
            array (
                'id' => 14,
                'group_id' => 8,
                'value' => 'Marketing',
                'locale' => 'en',
            ),
            11 => 
            array (
                'id' => 15,
                'group_id' => 8,
                'value' => 'تسويق',
                'locale' => 'ar',
            ),
            12 => 
            array (
                'id' => 16,
                'group_id' => 9,
                'value' => 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.',
                'locale' => 'en',
            ),
            13 => 
            array (
                'id' => 17,
                'group_id' => 9,
                'value' => 'نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.',
                'locale' => 'ar',
            ),
            14 => 
            array (
                'id' => 18,
                'group_id' => 10,
                'value' => 'Marketing',
                'locale' => 'en',
            ),
            15 => 
            array (
                'id' => 19,
                'group_id' => 10,
                'value' => 'تسويق',
                'locale' => 'ar',
            ),
            16 => 
            array (
                'id' => 20,
                'group_id' => 11,
                'value' => 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.',
                'locale' => 'en',
            ),
            17 => 
            array (
                'id' => 21,
                'group_id' => 11,
                'value' => 'نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.',
                'locale' => 'ar',
            ),
        ));
        
        
    }
}