<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('services')->delete();
        
        \DB::table('services')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => '10',
                'description' => '11',
                'home_image' => 'uploads/services/home_image_1549665773.png',
                'banner_image' => 'uploads/services/banner_image_1549665843.png',
                'page_image' => 'uploads/services/page_image_1549665843.png',
                'created_at' => '2019-02-08 22:33:34',
                'updated_at' => '2019-02-08 22:44:03',
            ),
        ));
        
        
    }
}