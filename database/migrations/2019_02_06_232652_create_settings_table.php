<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->text('address', 65535);
			$table->string('email');
			$table->string('phone1');
			$table->string('phone2');
			$table->text('fb_link', 65535);
			$table->text('twitter_link', 65535);
			$table->text('instagram_link', 65535);
			$table->text('linked_link', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
