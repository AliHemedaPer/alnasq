@extends('layouts.front')

@section('content')
<section class="our-works">
      <div class="container">
        <h2 class="text-header text-center">{{ $post->translate($share_locale)->title }}</h2>
        <p class="text-center"></p>
        <div class="row" style="padding-top: 30px">
           <div class="col-lg-8 col-md-12 col-sm-12">
              {!! $post->translate($share_locale)->description !!}
           </div>

           <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="image"><img class="img-thumbnail" src="{{url($post->image)}}"></div>
           </div>
        </div>
      </div>
    </section>
@stop