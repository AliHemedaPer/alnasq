@extends('layouts.front')

@section('content')
<section class="our-works">
      <div class="container">
        <h2 class="text-header text-center">@lang('frontend.case_study')</h2>
        <p class="text-center">@lang('frontend.case_study_hint')</p>
        <div class="row">

          @foreach($posts as $post)
            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card">
                <div class=""><img class="card-img-top" src="{{url($post->image)}}" alt="..."></div>
                <div class="card-body">
                  <h5 class="card-title">{{ $post->translate($share_locale)->title }}</h5>
                  <p class="card-text">{!! \Illuminate\Support\Str::words($post->translate($share_locale)->description, 20, ' ...') !!}</p>
                  <a class="btn btn-primary" href="{{ url('post/'.$post->id) }}">@lang('frontend.view')</a>
                </div>
              </div>
            </div>
          @endforeach

          <!-- <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
              <div class="image"><img class="card-img-top" src="{{url('public/front/')}}/images/Flati.webp" alt="..."></div>
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a class="btn btn-primary" href="#">Go somewhere</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
              <div class="image"><img class="card-img-top" src="{{url('public/front/')}}/images/Flati.webp" alt="..."></div>
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a class="btn btn-primary" href="#">Go somewhere</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
              <div class="image"><img class="card-img-top" src="{{url('public/front/')}}/images/Flati.webp" alt="..."></div>
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a class="btn btn-primary" href="#">Go somewhere</a>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </section>
@stop