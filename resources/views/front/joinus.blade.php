@extends('layouts.front')

@section('content')
<section class="our-works">
      <div class="container">
        <h2 class="text-header text-center">@lang('frontend.joinus')</h2>
        <p class="text-center">@lang('frontend.joinus_hint')</p>

        @if (session('status'))
          <div class="alert alert-success" role="alert">
            @lang('frontend.successmsg')
          </div>
        @endif

        <form method="post" action="{{ url('joinmail') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <!-- <label for="exampleInputUserName">User Name</label> -->
            <input class="form-control" name="name" type="text" id="exampleInputUserName" aria-describedby="UserName" placeholder="@lang('frontend.name')" required="">
          </div>
          <div class="form-group">
            <!-- <label for="exampleInputEmail1">Email address</label> -->
            <input class="form-control" name="email" type="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="@lang('frontend.email')" required="">
          </div>
          <div class="form-group">
            <!-- <label for="exampleInputTitleJob">Title Job</label> -->
            <input class="form-control" name="job" type="text" id="exampleInputTitleJob" aria-describedby="TitleJob" placeholder="@lang('frontend.job')" required="">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSubject">@lang('frontend.message')</label>
            <textarea class="form-control" name="message" id="exampleFormControlSubject" rows="3" required=""></textarea>
          </div>
          <div class="custom-file">
            <input class="custom-file-input" name="attachment" type="file" id="customFileLangHTML">
            <label class="custom-file-label" for="customFileLangHTML" data-browse="Upload">@lang('frontend.attach')</label>
          </div>
          <button class="choose-btn" type="submit">@lang('frontend.send')</button>
        </form>
      </div>
    </section>
@stop