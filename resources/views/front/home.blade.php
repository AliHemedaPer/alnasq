@extends('layouts.front')

@section('content')
    <!--Start Slider-move mouse-->
    <div id="particles-js"></div>
    <!--End Slider-move mouse-->
    <!--Start Slider text-->
    <section class="main-slider">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="image"><img src="{{url('public/front/')}}/images/logo.png" alt="Logo"></div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="carousel slide" id="carousel-One" data-ride="carousel">
              <div class="carousel-inner">
                @foreach($banners as $banner)
                  <div class="carousel-item {{ $loop->index == 0 ? 'active' : '' }}">
                    <div class="content">
                      <h1>{{ $banner->translate($share_locale)->title }}</h1>
                      <p>{{ $banner->translate($share_locale)->description }}</p>
                      @if($banner->url)
                      <a href="{{ url($banner->url) }}">@lang('frontend.details')</a>
                      @endif
                    </div>
                  </div>
                @endforeach
              </div>
              <ol class="carousel-indicators">
                @foreach($banners as $banner)
                  <li class="{{ $loop->index == 0 ? 'active' : '' }}" data-target="#carousel-One" data-slide-to="{{ $loop->index }}"></li>
                @endforeach
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--End Slider text-->
    <!--Start About-home-->
    <section class="about-home"> 
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-sm-12 about-home-text">
            <h2>{{ $about->translate($share_locale)->title }}</h2>
            <p>{{ $about->translate($share_locale)->short_description }}</p>
            <a class="choose-btn" href="{{ url('about') }}">@lang('frontend.more')</a>
          </div>
          <div class="col-md-7 col-sm-12 text-center about-home-image"><img src="{{ url($about->image) }}" alt=""></div>
        </div>
      </div>
    </section>
    <!--End About-home-->
    <!--Start background-->
    <section class="backGround">
      <!--Start ourServess-->
      <section class="services">
        <div class="container">
          <h2 class="text-header text-center">@lang('frontend.services')</h2>
          <p class="text-center">
            @lang('frontend.services_hint')
          </p>
          <div class="row">
            @if($share_locale == 'en')
              <div class="col-lg-4 col-md-6 col-sm-12 colm-card" data-aos="flip-left" data-aos-duration="1000">
                <div class="card-body">
                  <div class="flip-Card">
                    <div class="face">
                      <section class="image"><img src="{{url('public/front/')}}/images/design/Consulting.png"></section>
                      <h4>Consulting </h4>
                    </div>
                    <div class="back"> 
                      <p>To ensure success, one should cope up with the most up-to-date technology; that is why we adopt strategy.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 colm-card" data-aos="flip-left" data-aos-duration="1000">
                <div class="card-body">
                  <div class="flip-Card">
                    <div class="face"> 
                      <section class="image"><img src="{{url('public/front/')}}/images/design/Investment.png"></section>
                      <h4>Investment</h4>
                    </div>
                    <div class="back"> 
                      <p>People use digital products the same as any other kind of products. Therefore, we are not just interested in how it looks, but also in how it feels.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 colm-card" data-aos="flip-left" data-aos-duration="1000">
                <div class="card-body">
                  <div class="flip-Card">
                    <div class="face">
                      <section class="image"><img src="{{url('public/front/')}}/images/design/Marketing.png"></section>
                      <h4>Marketing </h4>
                    </div>
                    <div class="back"> 
                      <p>The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 colm-card" data-aos="flip-left" data-aos-duration="1000">
                <div class="card-body">
                  <div class="flip-Card">
                    <div class="face"> 
                      <section class="image"><img src="{{url('public/front/')}}/images/design/Design of application interfaces.png"></section>
                      <h4>Design interfaces</h4>
                    </div>
                    <div class="back"> 
                      <p>To ensure success, one should cope up with the most up-to-date technology; that is why we adopt strategy.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 olm-card" data-aos="flip-left" data-aos-duration="1000">
                <div class="card-body">
                  <div class="flip-Card">
                    <div class="face"> 
                      <section class="image"><img src="{{url('public/front/')}}/images/design/Application and site programming services.png"></section>
                      <h4>programming App</h4>
                    </div>
                    <div class="back"> 
                      <p>People use digital products the same as any other kind of products. Therefore, we are not just interested in how it looks, but also in how it feels.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 colm-card" data-aos="flip-left" data-aos-duration="1000">
                <div class="card-body">
                  <div class="flip-Card">
                    <div class="face">
                      <section class="image"><img src="{{url('public/front/')}}/images/design/Department of decoration design.png"></section>
                      <h4>Decoration  </h4>
                    </div>
                    <div class="back"> 
                      <p>The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.                </p>
                    </div>
                  </div>
                </div>
              </div>
              @else
                <div class="col-lg-4 col-md-6 col-sm-12 colm-card">
                  <div class="card-body">
                    <div class="flip-Card">
                      <div class="face">
                        <section class="image"><img src="{{url('public/front/')}}/images/design/Consulting.png"></section>
                        <h4>الاستشارات </h4>
                      </div>
                      <div class="back"> 
                        <p>نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 colm-card">
                  <div class="card-body">
                    <div class="flip-Card">
                      <div class="face"> 
                        <section class="image"><img src="{{url('public/front/')}}/images/design/Investment.png"></section>
                        <h4>استثمار</h4>
                      </div>
                      <div class="back"> 
                        <p>نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 colm-card">
                  <div class="card-body">
                    <div class="flip-Card">
                      <div class="face">
                        <section class="image"><img src="{{url('public/front/')}}/images/design/Marketing.png"></section>
                        <h4>تسويق </h4>
                      </div>
                      <div class="back"> 
                        <p>نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 colm-card">
                  <div class="card-body">
                    <div class="flip-Card">
                      <div class="face"> 
                        <section class="image"><img src="{{url('public/front/')}}/images/design/Design of application interfaces.png"></section>
                        <h4>واجهات تصميم</h4>
                      </div>
                      <div class="back"> 
                        <p>نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 olm-card">
                  <div class="card-body">
                    <div class="flip-Card">
                      <div class="face"> 
                        <section class="image"><img src="{{url('public/front/')}}/images/design/Application and site programming services.png"></section>
                        <h4>برمجة التطبيق</h4>
                      </div>
                      <div class="back"> 
                        <p>نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 colm-card">
                  <div class="card-body">
                    <div class="flip-Card">
                      <div class="face">
                        <section class="image"><img src="{{url('public/front/')}}/images/design/Department of decoration design.png"></section>
                        <h4>زخرفة</h4>
                      </div>
                      <div class="back"> 
                        <p>نقدم مجموعة من أفضل الأفكار والمنتجات وآليات التنفيذ التي توفر على المهتمين بالمجالات الخيرية الجهد والمال وعناء البحث عن سبل تنمية الموارد كما نسعى لابتكار طرق ووسائل جديدة تثري مجال العمل الخيري.</p>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
          </div>
        </div>
      </section>
      <!--End ourServess-->
      <div class="container">
        <hr>
      </div>
      <!--Start Works-->
      <section class="works">
        <div class="container">
          <a href="{{ url('ourworks') }}"><h2 class="text-header text-center">@lang('frontend.works')</h2></a>
          <p class="text-center">
            @lang('frontend.works_hint')
            </p>
          <div class="row"> 
            <div class="col-md-6 col-sm-12">
              <ul class="list-unstyled">
                @foreach($works as $work)
                  <li class="{{ $loop->index == 0 ? 'active' : '' }} works_tabs" data-aos="fade-up" data-aos-duration="1000" id="worktab-{{ $loop->index }}" onclick="viewwork('{{ $loop->index }}')"> <i class="fas fa-globe" i></i> {{ $work->translate($share_locale)->title }}</li>
                @endforeach
              </ul>
            </div>
            @foreach($works as $work)
              <div style="display: {{ $loop->index == 0 ? 'block' : 'none' }}" class="col-md-6 col-sm-12 work_images" data-aos="zoom-in" id="work-{{ $loop->index }}">
              <div class="veiw">
                <div class="image"><img src="{{url($work->image)}}"></div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>
      <!--End Works-->
      <!--Start Integrations icon-->
      <section class="Integrations"> 
        <div class="container">
          <h2 class="text-header text-center">@lang('frontend.statistics')</h2>
          <p class="text-center">
            @lang('frontend.statistics_hint')
          </p>
          <div class="row">
            @foreach($counters as $counter)
              <div class="col-lg col-md-3 col-sm-6 text-center" data-aos="zoom-in">
                <div class="icon"> 
                  <div class="image"><img src="{{ url($counter->image) }}" alt=""><span class="one"></span><span class="two"></span></div>
                  <h3> <span class="counter">{{ $counter->count }}</span></h3>
                  <p>{{ $counter->translate($share_locale)->title }}</p>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </section>
      <!--End Integrations icon-->
      <div class="container">
        <hr>
      </div>
      <!--Start Studies-->
      <section class="Studies">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <h2>@lang('frontend.case_study')</h2>
              <p>@lang('frontend.case_study_hint')</p><a class="choose-btn " href="{{ url('blog' ) }}">@lang('frontend.more')<i class="fas fa-angle-right"></i></a>
            </div>
            <div class="col-md-8">
              <div class="row">
                @foreach($posts as $post)
                  <div class="col-md-4" data-aos="fade-{{ $share_locale == 'ar' ? 'left' : 'right' }}"  data-aos-delay="{{ $loop->index * 150 }}">
                    <div class="card"><img class="card-img-top" src="{{ url($post->image) }}" alt="...">
                      <div class="card-body">
                        <h5 class="card-title">{{ $post->translate($share_locale)->title }}</h5>
                        <p class="card-text">{!! \Illuminate\Support\Str::words($post->translate($share_locale)->description, 20, ' ...') !!}</p><a class="btn btn-primary" href="{{ url('post/'.$post->id ) }}">@lang('frontend.details')</a>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--End Studies-->
      <div class="container">
        <hr>
      </div>
      <!--Start Prices List-->
      <!--section.pricesLists 
      .container
          h2(class="text-header text-center") MONTHLY 
              span SUBSCRIPTIONS
          p.text-center A 30 days free trial for all. A brief story about how this process works, keep an eye till the end.
          .row
              div(class="col-lg-4 col-md-12 col-sm-12 text-center list-Price-item" data-aos="fade-up" data-aos-duration="1000")
                  .list-Price
                      h3 SILVER
                      .price 
                          p $39
                          span per month
                      ul(class="list-unstyled item")
                          li 
                              i(class="far fa-check-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-check-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-times-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-times-circle")
                              |  Lorem ipsum dolor sit am
                      a(href="" class="choose-btn") CHOOSE PLAN             
              div(class="col-lg-4 col-md-12 col-sm-12 text-center list-Price-item" data-aos="fade-up" data-aos-delay="150" data-aos-duration="1000")
                  .list-Price
                      h3 GOLD
                      .price 
                          p $39
                          span per month
                      ul(class="list-unstyled item")
                          li 
                              i(class="far fa-check-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-check-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-times-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-times-circle")
                              |  Lorem ipsum dolor sit am
                      a(href="" class="choose-btn") CHOOSE PLAN            
              div(class="col-lg-4 col-md-12 col-sm-12 text-center list-Price-item" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000")
                  .list-Price
                      h3 PLATINUM
                      .price 
                          p $39
                          span per month
                      ul(class="list-unstyled item")
                          li 
                              i(class="far fa-check-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-check-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-times-circle")
                              |  Lorem ipsum dolor sit am
                          li 
                              i(class="far fa-times-circle")
                              |  Lorem ipsum dolor sit am
                      a(href="" class="choose-btn") CHOOSE PLAN
      -->
      <!--End Prices List                    -->
      <!--Start Partners-->
      <section class="Partners"> 
        <div class="container">
          <h2 class="text-header text-center">@lang('frontend.integrations')</h2>
          <p class="text-center">
            @lang('frontend.integrations_hint')
          </p>
          <div class="carousel slide" id="carouselExampleIndicators" data-ride="carousel">
            <div class="carousel-inner">
              @foreach($logos as $key => $logo)
              <div class="carousel-item {{ $loop->index == 0 ? 'active':'' }}">
                <div class="row text-center">
                    @foreach($logo as $k => $image)
                      <div class="col-lg-3">
                        <div class="logo"><img src="{{ url($image) }}" alt=""></div>
                      </div>
                    @endforeach
                </div>
              </div>
              @endforeach
            </div>
            <ol class="carousel-indicators">
              @foreach($logos as $key => $logo)
              <li class="{{ $loop->index == 0 ? 'active':'' }}" data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}"></li>
              @endforeach
            </ol>
          </div>
        </div>
      </section>
      <!--End Partners-->
      <!--Start Team-->
      <section class="team"> 
        <div class="container">
          <h2 class="text-header text-center">@lang('frontend.team')</h2>
          <p class="text-center">
            @lang('frontend.team_hint')
          </p>
          <div class="carousel slide" id="TEAM2" data-ride="carousel">
            <div class="carousel-inner">
              @foreach($team as $key => $list)
              <div class="carousel-item {{ $loop->index == 0 ? 'active':'' }}">
                <div class="row text-center">
                    @foreach($teamCollection as $member)
                      @if(in_array($member->id , $list))
                      <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="team-item"><img src="{{ url($member->image) }}" alt="">
                          <h3>{{ $member->translate($share_locale)->name }}</h3>
                          <h5>{{ $member->translate($share_locale)->position }}</h5>
                          <p>{{ $member->translate($share_locale)->description }}</p>
                        </div>
                      </div>
                      @endif
                    @endforeach
                </div>
              </div>
              @endforeach

            </div>
            <ol class="carousel-indicators">
              @foreach($team as $list)
              <li class="{{ $loop->index == 0 ? 'active':'' }}" data-target="#TEAM2" data-slide-to="{{ $loop->index }}"></li>
              @endforeach
            </ol>
          </div>
        </div>
      </section>
      <!--Start Team-->
    </section>
    <!--Start Contact-home-->
    <section class="Contact-home" id="contactus" style="position: relative;">
      <ul class="social-nav model-3d-0 footer-social social two" style="    position: absolute;
    {{ $share_locale == 'ar' ? 'right' : 'left' }}: -25px;
    top: 144px;
    z-index: 999;">
        <li><a class="facebook" href="#">
            <div class="front"><i class="fab fa-facebook-f"></i></div>
            <div class="back"><i class="fab fa-facebook-f"></i></div></a></li>
        <li><a class="twitter" href="#">
            <div class="front"><i class="fab fa-twitter"></i></div>
            <div class="back"><i class="fab fa-twitter">   </i></div></a></li>
        <li><a class="instagram" href="#">
            <div class="front"><i class="fab fa-instagram"></i></div>
            <div class="back"><i class="fab fa-instagram"> </i></div></a></li>
        <li><a class="behance" href="#">
            <div class="front"><i class="fab fa-behance"></i></div>
            <div class="back"><i class="fab fa-behance">           </i></div></a></li>
        <li><a class="linkedin" href="#">
            <div class="front"><i class="fab fa-linkedin-in"></i></div>
            <div class="back"><i class="fab fa-linkedin-in">                    </i></div></a></li>
      </ul>

      <div class="container">
        <h2 class="text-header text-center">@lang('frontend.touch')</h2>
        <p class="text-center">@lang('frontend.touch_hint')</p>
        <div class="row">
          <div class="col-lg-4 col-md-12 col-sm-12  ">
            <div class="Contact-call">
              <h4>@lang('frontend.ouraddress')</h4>
              <p> <i class="fas fa-map-marker-alt"></i>@lang('frontend.theaddress')</p>
              <h4>@lang('frontend.call')</h4>
              <p> <i class="fas fa-mobile-alt"></i>+966543741157</p>
              <p> <i class="fas fa-mobile-alt"></i>+966566635338</p>
              <h4>@lang('frontend.emailus')</h4>
              <p> <i class="fas fa-envelope"></i>info@alnasqarb.com</p>
              <h4>  <a href="{{ url('joinus') }}">@lang('frontend.join')<i class="fas fa-hand-point-left"></i></a></h4>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12  ">
            <form method="post" action="{{ url('contactmail') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <input class="form-control" name="name" type="text" id="exampleInputname1" aria-describedby="NameHelp" placeholder="@lang('frontend.name')" required="">
              </div>
              <div class="form-group">
                <input class="form-control" name="email" type="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="@lang('frontend.email')"  required="">
              </div>
              <div class="form-group">
                <input class="form-control" name="subject" type="text" id="exampleInputSubject1" aria-describedby="SubjectHelp" placeholder="@lang('frontend.subject')"  required="">
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" aria-label="With textarea" id="exampleInputMessage1" placeholder="@lang('frontend.message')" rows="4"  required=""></textarea>
              </div>
              <button class="choose-btn" type="submit">@lang('frontend.submit')</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!--End Contact-home-->
@stop