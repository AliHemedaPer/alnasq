@extends('layouts.front')

@section('content')
    <section class="slider-img-about">
      <div class="container">
        <!--Start Design-->
        <section class="our-about">
          <h2 class="text-header text-center">@lang('frontend.aboutus')</h2>
          <p class="text-center">@lang('frontend.aboutus_hint')</p>
          <div class="row">
            <div class="col-md-5 col-sm-12 about-home-text">
              <h2>{{ $about->translate($share_locale)->title }}</h2>
              {{ $about->translate($share_locale)->short_description }}
            </div>
            <div class="col-md-7 col-sm-12 text-center about-home-image"><img src="{{ url($about->image) }}" alt=""></div>
          </div>
        </section>
        <!--End Works-->
      </div>
    </section>
@stop