@extends('layouts.front')

@section('content')
<section class="our-works">
      <div class="container">
        <h2 class="text-header text-center">@lang('frontend.works')</h2>
        <p class="text-center">@lang('frontend.works_hint')</p>

        <form>
          <div class="col-lg-5 col-md-5 col-sm-12" style="float: {{ $share_locale == 'ar' ? 'right':'left'}};">
            <div class="form-group">
              <label for="exampleInputUserName">@lang('frontend.category')</label>
              <select class="form-control" name='category' id="cat_id"  onchange="getSubcats(this.value)">
                <option value="0">--- @lang('frontend.all') ---</option>
                @foreach($categories as $parent)
                  <option value="{{ $parent->id }}" @if(isset($category) && $category == $parent->id) selected="" @endif>{{ $parent->translate($share_locale)->title }}</option>
              @endforeach
              </select>
            </div>
          </div>
          <div class="col-lg-5 col-md-5 col-sm-12" style="float: {{ $share_locale == 'ar' ? 'right':'left'}};">
            <div class="form-group">
              <label for="exampleInputUserName">@lang('frontend.subcategory')</label>
              <select class="form-control" name='subcategory' id="subcat_id">
                <option value="0">--- @lang('frontend.all') ---</option>
              </select>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-12" style="float: {{ $share_locale == 'ar' ? 'right':'left'}};">
            <button class="choose-btn" type="submit">@lang('frontend.filter')</button>
          </div>
          <div style="clear: both;"></div>
        </form>
        <hr/>
        <div class="row">

          @foreach($works as $work)
            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card">
                <div class="image"><img class="card-img-top" src="{{url($work->image)}}" alt="..."></div>
                <div class="card-body">
                  <h5 class="card-title">{{ $work->translate($share_locale)->title }}</h5>
                  <p class="card-text">{!! \Illuminate\Support\Str::words($work->translate($share_locale)->description, 20, ' ...') !!}</p>
                  @if($work->link)
                    <a class="btn btn-primary" href="{{ url($work->link) }}">@lang('frontend.view')</a>
                  @endif
                </div>
              </div>
            </div>
          @endforeach

          <!-- <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
              <div class="image"><img class="card-img-top" src="{{url('public/front/')}}/images/Flati.webp" alt="..."></div>
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a class="btn btn-primary" href="#">Go somewhere</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
              <div class="image"><img class="card-img-top" src="{{url('public/front/')}}/images/Flati.webp" alt="..."></div>
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a class="btn btn-primary" href="#">Go somewhere</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
              <div class="image"><img class="card-img-top" src="{{url('public/front/')}}/images/Flati.webp" alt="..."></div>
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a class="btn btn-primary" href="#">Go somewhere</a>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </section>
@stop