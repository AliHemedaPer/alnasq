@extends('layouts.front')

@section('content')
<section class="Design">
      <div class="container"> 
        <div class="row"> 
          @if($service->page_image)
          <div class="col-lg-6 col-md-12 col-sm-12 ">
          @else
          <div class="col-lg-12 col-md-12 col-sm-12 ">
          @endif
            <h2>{{ $title[0] }} <span> @for ($i = 1; $i < count($title); $i++)
                                            {{ $title[$i] }}
                                        @endfor
                                </span></h2>
            <p>{{ $service->translate($share_locale)->description }}</p>
            <!--Button trigger modal--><a class="choose-btn" href="" data-toggle="modal" data-target="#exampleModal">@lang('frontend.buy')</a>
            
          </div>
          @if($service->page_image)
            <div class="col-lg-6 col-md-12 col-sm-12 text-center">
              <div class="image"><img class="img-thumbnail" src="{{url($service->page_image)}}"></div>
            </div>
          @endif
          
        </div>
      </div>

      <!--Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" div>
              <h5 class="modal-title" id="exampleModalLabel">{{ $service->translate($share_locale)->title }}</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <input type="hidden" id="service_title" value="{{ $service->translate($share_locale)->title }}">
              <div class="form-group">
                <input class="form-control" type="text" id="service_uname" aria-describedby="UserName" placeholder="@lang('frontend.name')">
              </div>
              <div class="form-group">
                <input class="form-control" type="email" id="service_email" aria-describedby="emailHelp" placeholder="@lang('frontend.email')">
              </div>
              <div class="form-group">
                <input class="form-control" type="text" id="service_phone" aria-describedby="TitleJob" placeholder="@lang('frontend.phone')">
              </div>
              <div class="form-group">
                <label for="exampleFormControlSubject">@lang('frontend.message')</label>
                <textarea class="form-control" id="service_message" rows="3"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <span style="color:#e69929;display: none;" id="service_sending">@lang('frontend.sending')...</span>
              <button class="btn btn-secondary" type="button" data-dismiss="modal">@lang('frontend.close')</button>
              <button class="btn btn-primary" type="button" onclick="servicemail()">@lang('frontend.send')</button>
            </div>
          </div>
        </div>
      </div>
    </section>
@stop