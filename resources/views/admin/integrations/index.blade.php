@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="#">
            الرئيسية
        </a>
    </li>
    <li class="active">
        العملاء
    </li>

</ol>
<div class="page-header">
    <h1 class="col-md-6">العملاء</h1>
    <div class="col-md-6">
        <a class="btn btn-success pull-left" href="{{url("admin/integrations/create")}}"><i class="icon-plus2 mr-2"></i> إضافة عميل <i class="fa fa-plus"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    @foreach($integrations as $k => $value)
                    <div class="col-md-3 col-sm-4 gallery-img">
                        <div class="wrap-image">
                            <a class="group1" href="{{url($value->image)}}" title="">
                                <img src="{{url($value->image)}}" alt="" class="img-responsive">
                            </a>
                            <div class="chkbox"></div>
                            <div class="tools tools-bottom">                               
                                <a href="{{url('admin/integrations/'.$value->id.'/edit')}}">
                                    <i class="clip-pencil-3 "></i>
                                </a>
                                {{ Form::open(array('url' => 'admin/integrations/' . $value->id, 'style' => 'display: inline-block;')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <button type="submit" class="btn btn-danger btn-sm"onclick="return confirm('are you sure?')" ><i class="clip-close-2"></i></button>
                            {{ Form::close() }}
                               
                            </div>
                        </div>
                    </div>
                    @endforeach         


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="{{url('public/admin/')}}/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
jQuery(document).ready(function () {

    TableData.init();
});
</script>
@endsection