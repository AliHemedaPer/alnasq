<div class="col-lg-12"> 
        <div class="form-group">
        <label for="form-field-select-3">
            القسم الرئيسي
        </label>
        <select id="form-field-select-3" class="form-control search-select" name="parent_id">
            <option value="0">-- بدون --</option>
            @foreach($parents as $parent)
                <option value="{{ $parent->id }}" @if(isset($category) && $category->parent_id == $parent->id) selected="" @endif>{{ $parent->translate('ar')->title }}</option>
            @endforeach
        </select>
    </div>
</div>

<div style="clear: both;"></div>

<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example1" data-toggle="tab">
                العنوان (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example1" data-toggle="tab">
                العنوان (AR)
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example1">
            {!! Form::text('title[en]',($category)?$category->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example1">
            {!! Form::text('title[ar]',($category)?$category->translate('ar')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>
<div class="row"> 
    <div class="col-lg-3 col-lg-pull-9" style="text-align: left;">       
        <button type="submit" class="btn btn-success">{{$buttonAction}} <i class="fa fa-check"></i></button>
        <a class="btn btn-danger" href="{{url("admin/categories")}}"><i class="icon-plus2 mr-2"></i>إلغاء <i class="fa fa-minus-circle"></i></a>
    </div>
</div>
