@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            الرئيسية
        </a>
    </li>
    <li>
        <a href="{{url('admin/works')}}">
            أعمالنا
        </a>
    </li>
    <li class="active">
        @if($work)
        تعديل
        @else 
        إضافة
        @endif
    </li>
  
</ol>
<div class="page-header">
    <h1 class="col-md-6">أعمالنا</h1>
    <div class="col-md-6">
        <a class="btn btn-primary pull-left" href="{{url("admin/works")}}"><i class="icon-plus2 mr-2"></i> رجوع <i class="fa fa-reply"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            
        <div class="panel-body">
        @if($work)
        {!! Form::model($work,["url"=>"admin/works/$work->id","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
        @include('admin.works.input',['buttonAction'=>'حفظ'])
        @else 
        {!! Form::open(["url"=>"admin/works","class"=>"form-horizontal","method"=>"POST","enctype"=>"multipart/form-data"]) !!}
        @include('admin.works.input',['buttonAction'=>'حفظ'])
        @endif
        {!! Form::close() !!}
    </div>
</div>
</div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({
            allowClear: true
        });
        FormElements.init();
    });

    @if(isset($work) && $work->cat_id)
        getSubcats({{ $work->cat_id }}, {{ $work->subcat_id }})
    @endif
    function getSubcats(cat_id, subcat_id = 0){
        let url = '{{ url('') }}' + '/subcategories/' + cat_id;
        jQuery('#subcat_id').html('<option value="0">--- إختيار --</option>');
        //jQuery('#loader').fadeIn();
        jQuery.get(url, function(data) {

            jQuery.each(data,function(key, value) {
                let selected = '';
                if(subcat_id == value.id)
                    selected = 'selected';
                
                jQuery('#subcat_id').append('<option value="' + value.id + '"'+ selected +' >' + value.value + '</option>');
               
            });
             //jQuery('#loader').fadeOut(10);
        });
    }
</script>
@endsection