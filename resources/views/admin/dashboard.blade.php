@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li class="active">
        <i class="clip-home-3"></i>
            الرئيسية
    </li>
</ol>
<div class="page-header">
    
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-info circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/about')}}">عن النسق العربي</a></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-star-4 circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/services')}}">الخدمات</a></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-grid circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/categories')}}">الأقسام</a></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-stack-empty circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/works')}}">أعمالنا</a></h2>
			</div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-left-quote circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/banners')}}">نصوص البانر</a></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-stats circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/counters')}}">العدادات</a></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-book circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/posts')}}">البلوج</a></h2>
			</div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-user-3 circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/team')}}">فريق العمل</a></h2>
			</div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="core-box">
			<div class="heading">
				<i class=" clip-user-5 circle-icon circle-teal"></i>
				<h2><a href="{{url('admin/integrations')}}">العملاء</a></h2>
			</div>
		</div>
	</div>
</div>
@stop