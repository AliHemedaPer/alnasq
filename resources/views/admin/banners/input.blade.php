<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example1" data-toggle="tab">
             العنوان (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example1" data-toggle="tab">
             العنوان (AR)
            </a>
        </li>
      
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example1">
             {!! Form::text('title[en]',($banner)?$banner->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example1">
            {!! Form::text('title[ar]',($banner)?$banner->translate('ar')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>
<div class="tabbable">
    <ul id="myTab1" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example2" data-toggle="tab">
           الوصف (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example2" data-toggle="tab">
            الوصف (AR)
            </a>
        </li>
      
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example2">
            {!! Form::textarea('description[en]',($banner)?$banner->translate('en')->description:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example2">
            {!! Form::textarea('description[ar]',($banner)?$banner->translate('ar')->description:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>

<div class="form-group row text-center">
    <label class="col-form-label col-lg-2" style="text-align:center">الرابط <span class="text-danger">*</span></label>
    <div class="col-lg-10">       
        {!! Form::text('url',null,['class'=>'form-control']) !!}
    </div>
</div>
<div class="row"> 
    <div class="col-lg-3 col-lg-pull-9" style="text-align: left;">       
    <button type="submit" class="btn btn-success">{{$buttonAction}} <i class="fa fa-check"></i></button>
    <a class="btn btn-danger" href="{{url("admin/banners")}}"><i class="icon-plus2 mr-2"></i>إلغاء <i class="fa fa-minus-circle"></i></a>
</div>
</div>
