@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            الرئيسية
        </a>
    </li>
    <li>
        <a href="{{url('admin/banners')}}">
            نصوص البانر
        </a>
    </li>
    <li class="active">
        @if($banner)
        تعديل
        @else 
        إضافة
        @endif
    </li>
  
</ol>
<div class="page-header">
    <h1 class="col-md-6">نصوص البانر</h1>
    <div class="col-md-6">
        <a class="btn btn-primary pull-left" href="{{url("admin/banners")}}"><i class="icon-plus2 mr-2"></i> رجوع <i class="fa fa-reply"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            
            <div class="panel-body">
        @if($banner)
        {!! Form::model($banner,["url"=>"admin/banners/$banner->id","class"=>"form-horizontal","method"=>"patch"]) !!}
        @include('admin.banners.input',['buttonAction'=>'حفظ'])
        @else 
        {!! Form::open(["url"=>"admin/banners","class"=>"form-horizontal","method"=>"POST"]) !!}
        @include('admin.banners.input',['buttonAction'=>'حفظ'])
        @endif
        {!! Form::close() !!}
    </div>
</div>
</div>
</div>

@endsection