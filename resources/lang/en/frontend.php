<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'home' => 'Home',
    'about' => 'About Us',
    'services' => 'Services',
    'works' => 'Works',
    'contact' => 'Contact Us',
    'language' => 'عربي',
    'details' => 'Details',
    'more' => 'Read More',
    'services' => 'Our <span>services</span>',
    'services_hint' => 'Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. <br/>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'works' => 'Our <span>works</span>',
    'works_hint' => 'Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. <br/>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'integrations' => 'Great Integrations <span>with Others</span>',
    'integrations_hint' => 'Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. <br/>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'case_study' => 'Case Studies',
    'case_study_hint' => 'Our duty towards you is to share our experience we\'re reaching in our work path with you.',
    'statistics' => 'Statistics',
    'statistics_hint' => 'Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. <br/>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'team' => 'OUR  <span>TEAM</span>',
    'team_hint' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem. <br/>accusantium doloremque laudantium totam rem aperiam, eaque ipsa .',
    'touch' => 'KEEP IN <span>TOUCH</span>',
    'touch_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. ',
    'ouraddress' => 'Our Address',
    'address' => 'House #13, Streat road, Sydney 2310 Australia',
    'call' => 'Call Us',
    'emailus' => 'Email Us',
    'join' => 'Join us',
    'name' => 'Name',
    'email' => 'Email',
    'subject' => 'Subject',
    'message' => 'Message',
    'submit' => 'Submit',
    'poweredby' => 'Powered by &copy; Alnasq Alarabi Co.',
    'aboutus' => 'About <span>Us</span>',
    'aboutus_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. ',
    'buy' => 'Buy Now',
    'phone' => 'Phone',
    'sending' => 'Sending',
    'close' => 'Close',
    'send' => 'Send',
    'view' => 'View',
    'joinus' => 'Join <span>us</span>',
    'joinus_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'job' => 'Job Title',
    'attach' => 'Attach',
    'send' => 'Send',
    'category' => 'Category',
    'subcategory' => 'Sub Category',
    'filter' => 'Filter',
    'all' => 'All'
];
