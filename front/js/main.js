/*global $, document, AOS,window*/
$(document).ready(function () {
    "use strict";
    /* when click on any li in navbar scrollbutton on your ID */
	$(".navbar-nav .nav-item .nav-link ").click(function () {
		$("html, body").animate({
			scrollTop : $("#" + $(this).data("vale")).offset().top
		}, 2000);
	});
    $('.carousel').carousel({
        interval: 3000
    });
    AOS.init();
      
    /*Loader*/
    $(window).on('load', function () {
        $("body").css("overflow", "auto");
        $(".loader").fadeOut(3000);
    });
});