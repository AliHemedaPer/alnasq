<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('home');
Route::get('/home', 'FrontController@index')->name('home');
Route::get('/about', 'FrontController@about')->name('about');
Route::get('/services/{service}', 'FrontController@services')->name('services');
Route::get('/ourworks', 'FrontController@ourworks')->name('ourworks');
Route::get('/joinus', 'FrontController@joinus')->name('joinus');

Route::get('/blog', 'FrontController@blog')->name('blog');
Route::get('/post/{post}', 'FrontController@post')->name('post');

Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
});
Route::post('/servicemail', 'FrontController@servicemail')->name('servicemail');
Route::post('/contactmail', 'FrontController@contactmail')->name('contactmail');
Route::post('/joinmail', 'FrontController@joinmail')->name('joinmail');

Route::get('/subcategories/{cat_id}', 'FrontController@getSubcats');
///////////Admin
Route::group(['prefix' => 'admin'], function () {
	Auth::routes();
    Route::get('/logout', 'Auth\LoginController@logout');
});
Route::group(['prefix' => 'admin','middleware' => ['auth']], function () {
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::resource('banners', 'Admin\BannerController');
    Route::get('/about', 'Admin\AboutController@index');
    Route::patch('/about', 'Admin\AboutController@update');
    Route::resource('services', 'Admin\ServiceController');
    Route::resource('integrations', 'Admin\IntegrationController');
    Route::resource('categories', 'Admin\CategoriesController');
    Route::resource('works', 'Admin\WorksController');
    Route::resource('team', 'Admin\TeamController');
    Route::resource('counters', 'Admin\CountersController');
    Route::resource('posts', 'Admin\PostsController');
});


