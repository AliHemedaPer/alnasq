<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin\Service;
use App\Admin\Banner;
use App\Admin\About;
use App\Admin\Works;
use App\Admin\Counters;
use App\Admin\Posts;
use App\Admin\Integration;
use App\Admin\Team;
use App\Admin\Categories;

use App;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class FrontController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
    //     $locale = App::getLocale();
    //     echo $locale;
    //     exit();
        $ishome   = 1;
        $banners  = Banner::get();
        $about    = About::find(1);
        $works    = Works::limit(4)->latest()->get();
        $counters = Counters::get();
        $posts    = Posts::limit(3)->latest()->get();
        $team     = Team::get();
        
        //logos to group of 4s
        $Alllogos    = Integration::get()->toArray();
        $logoGroups = ceil(count($Alllogos)/4);
        $logos = [];
        $j = 0;
        $k = 0;
        for($i=0; $i < $logoGroups; $i++){
            for($j; $j < count($Alllogos); $j++){
                if($k > 0 && $k%4 == 0){
                    $k = 0;
                    $j--;
                    break;
                }
                if(isset($Alllogos[$i+$j])){
                   $k++; 
                   $logos[$i][] = $Alllogos[$i+$j]['image'];
                }
                
            }
        }
        ////

        //Team to group of 4s
        $teamCollection    = Team::get();
        $Allteam    = $teamCollection->toArray();
        $teamGroups = ceil(count($Allteam)/4);
        $team = [];
        $j = 0;
        $k = 0;
        for($i=0; $i < $teamGroups; $i++){
            for($j; $j < count($Allteam); $j++){
                if($k > 0 && $k%4 == 0){
                    $k = 0;
                    $j--;
                    break;
                }
                if(isset($Allteam[$i+$j])){
                   $k++; 
                   $team[$i][] = $Allteam[$i+$j]['id'];
                }
                
            }
        }
        ////
        return view('front.home')->with(compact('ishome', 'banners', 'about', 'works', 'counters' ,'posts', 'logos', 'team', 'teamCollection'));
    }

    public function about()
    {
        $about = About::find(1);
        return view('front.about')->with(compact('about'));
    }

    public function services(Service $service)
    {
        $lang = App::getLocale();
        $top_slider_img = 'slider-img';
        $title = explode(' ', $service->translate($lang)->title);
        
        //used in master
        $bannerBkg = $service->banner_image ? $service->banner_image : null;
        ///////////////

        return view('front.service')->with(compact('service', 'title', 'bannerBkg',  'top_slider_img'));
    }

    public function ourworks(Request $request)
    {
        $category = 0;
        $subcategory = 0;

        $top_slider_img = 'slider-img-works';

        $workQuery = Works::where('id', '>' , '0');

        if($request->category){
            $category = $request->category;
            $workQuery->where('cat_id', '=', $category);
        }

        if($request->subcategory){
            $subcategory = $request->subcategory;
            $workQuery->where('subcat_id', '=', $subcategory);
        }
        
        $works = $workQuery->latest()->get();
        
        $categories = Categories::where('parent_id', '=', 0)->get();

        return view('front.ourworks')->with(compact('works', 'categories', 'category', 'subcategory', 'top_slider_img'));
    }

    public function joinus()
    {
        $top_slider_img = 'slider-img-Joinus';
        return view('front.joinus')->with(compact('top_slider_img'));
    }

    public function blog()
    {
        $top_slider_img = 'slider-img-blog';

        $posts = Posts::get();
        return view('front.blog')->with(compact('posts', 'top_slider_img'));
    }

    public function post(Posts $post)
    {
        $top_slider_img = 'slider-img-blog';
        return view('front.blogpost')->with(compact('post', 'top_slider_img'));
    }

    public function servicemail(Request $request){
        //$msgStatus = -1;
        $toMail = config('mail.to');
        $title  = $request->title;

        $data = ['title' => $title, 'email' => $request->email, 'name' => $request->uname, 'phone' => $request->phone, 'message' => $request->message];

        $mail = Mail::send('emails.service', ['data' => $data], function ($m) use ($toMail, $title) {
            $m->to($toMail)->subject($title.' - Product Buy!');
        });
        //if($mail) {
            $msgStatus = 1;
        //}
        return response()->json([
            'status' => $msgStatus
        ]);
    }

    public function contactmail(Request $request){

        $toMail = config('mail.to');
        $subject = $request->subject;

        $data = ['email' => $request->email, 'name' => $request->name, 'subject' => $subject, 'message' => $request->message];

        $mail = Mail::send('emails.contact', ['data' => $data], function ($m) use ($toMail, $subject) {
            $m->to($toMail)->subject($subject);
        });
        return redirect('home');
    }

    public function joinmail(Request $request){

        $toMail = config('mail.to');
        $job = $request->job;

        $path     = "uploads/attachment/";
        $filepath = '';
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $uniqueID = time();
            $file_name = 'attachment_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $filepath = $path . $file_name;
        }
        
        $data = ['email' => $request->email, 'name' => $request->name, 'job' => $job, 'message' => $request->message];

        $mail = Mail::send('emails.joinus', ['data' => $data], function ($m) use ($toMail, $filepath) {
            $m->to($toMail)->subject('Join Us - Request!');
            if($filepath)
                $m->attach($filepath);
        });
        $status = 1;
        return redirect('joinus')->with('status', '1');
    }

    public function getSubcats($cat_id){
        $lang = App::getLocale();
        $subs = DB::table('categories as c')->select('c.id', 't.value')->leftjoin('translations as t', 'c.title' ,'=', 't.group_id')->where('c.parent_id','=', $cat_id)->where('t.locale','=', $lang )->get();
        return response()->json($subs);
    }
}
