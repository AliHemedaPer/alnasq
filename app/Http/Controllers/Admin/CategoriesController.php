<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Categories;
use Validator;
use DB;

class CategoriesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = Categories::all();
        
        $parents = DB::table('categories as c')->select('c.id', 't.value')->leftjoin('translations as t', 'c.title' ,'=', 't.group_id')->get()->pluck('value', 'id')->toArray();

        return view('admin.categories.index', compact('categories', 'parents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $category = NULL;
        $parents = Categories::where('parent_id', '=', 0)->get();
        return view('admin.categories.form', compact('category', 'parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        
        Categories::create($data);

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categories $category) {
        $parents = Categories::where('parent_id', '=', 0)->where('id', '!=', $category->id)->get();
        return view('admin.categories.form', compact('category', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $category) {
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $category->update($request->all());
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $category) {
        $parents = Categories::where('parent_id', '=', $category->id)->count();
        if($parents){
            \Session::flash('error', 'خطأ, يحتوي علي أقسام فرعيه!');
            return redirect('admin/categories');
        }
        $category->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/categories');
    }

}
