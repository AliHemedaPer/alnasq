<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\About;
use Validator;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = About::Find(1);        
        return view('admin.about.form', compact('info'));
    }

    public function update(Request $request)
    {
         $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'short_description.*' => 'required',
                    'content.*' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 
        $info = About::Find(1);
        $NewInfo = $request->all();
        if ($request->hasFile('image')) {
                $file = $request->file('image');
                $path = "uploads/AboutPage/";
                $uniqueID = time();
                $file_name = $uniqueID . "." . $file->getClientOriginalExtension();
                $file->move($path, $file_name);
                if (file_exists($info->image)) {
                    unlink($info->image);
                }
                $NewInfo['image'] = $path . $file_name;
            }
        $info->update($NewInfo);
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/about');
    }

}
