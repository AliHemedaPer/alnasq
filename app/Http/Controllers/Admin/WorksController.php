<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Categories;
use App\Admin\Works;
use Validator;
use DB;

class WorksController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $works = Works::all();
        return view('admin.works.index', compact('works'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $work = NULL;
        $categories = Categories::where('parent_id', '=', 0)->get();
        //$parents = DB::table('categories as c')->select('c.id', 't.value')->leftjoin('translations as t', 'c.title' ,'=', 't.group_id')->get()->pluck('value', 'id')->toArray();
        return view('admin.works.form', compact('work', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/works/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['image'] = $path . $file_name;
        }
        Works::create($data);

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/works');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Works $work) {
        $categories = Categories::where('parent_id', '=', 0)->get();
        return view('admin.works.form', compact('work', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Works $work) {
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/works/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($work->image)) {
                unlink($work->image);
            }
            $data['image'] = $path . $file_name;
        }
        $work->update($data);
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/works');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Works $work) {
        if (file_exists($work->image)) {
            unlink($work->image);
        }
        $work->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/works');
    }

}
