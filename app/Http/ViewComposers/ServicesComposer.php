<?php 

namespace App\Http\ViewComposers;

use App;
use Illuminate\Contracts\View\View;
use App\Admin\Service;

class ServicesComposer
{
    public function compose(View $view) {
    	$locale   = App::getLocale();
    	$services = Service::get();
        $view->with('share_services', $services);
        $view->with('share_locale', $locale);
    }
}