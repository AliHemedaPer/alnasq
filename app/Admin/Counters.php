<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Counters extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title'];
    protected $fillable = ['title', 'count', 'image'];
    protected $table = 'counters';
}
