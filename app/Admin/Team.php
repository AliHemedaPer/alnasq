<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['name', 'position', 'description'];
    protected $fillable = ['name', 'position', 'description', 'image'];
    protected $table = 'team';
}
