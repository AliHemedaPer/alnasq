<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title'];
    protected $fillable = ['title', 'parent_id'];
    protected $table = 'categories';
}
