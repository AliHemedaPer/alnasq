<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title','short_description','content'];
    
     protected $fillable = ['title','short_description','content','image'];
     
     protected $table = 'about';
}
