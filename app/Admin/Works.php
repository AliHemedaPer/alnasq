<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Works extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title', 'description'];
    protected $fillable = ['title', 'cat_id', 'subcat_id', 'title', 'description', 'image', 'link'];
    protected $table = 'works';
}
