<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title', 'description'];
    protected $fillable = ['title', 'description', 'image' ];
    protected $table = 'posts';
}
