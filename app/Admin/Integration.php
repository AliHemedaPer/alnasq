<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model {

    protected $fillable = ['image'];

}
